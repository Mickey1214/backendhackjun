package techu.hack.poliza.servicio;

import org.springframework.data.mongodb.repository.MongoRepository;
import techu.hack.poliza.modelo.Cliente;

public interface RepositorioCliente extends MongoRepository<Cliente, String> {
}
