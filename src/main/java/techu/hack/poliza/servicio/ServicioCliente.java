package techu.hack.poliza.servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import techu.hack.poliza.modelo.Cliente;
import techu.hack.poliza.modelo.Poliza;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ServicioCliente {
    @Autowired
    RepositorioCliente clienteRepository;

    //Busca todos los clientes
    public List<Cliente> findAll(){
        return clienteRepository.findAll();
    }

    //Buscar todas las polizas de un cliente
    public  List<Poliza> findAllPolizas(String id){
        Optional<Cliente> cliente = clienteRepository.findById(id);
        System.out.println("id del cliente: " + cliente.isEmpty());
        if(cliente != null){
            return cliente.get().getNumero_poliza();
        }
        else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    //Busca un cliente en especifico
    public Cliente findById(String  id) {
        Optional<Cliente> c = this.clienteRepository.findById(id);
        return c.isPresent() ? c.get() : null;
    }

    //Guarda un recurso cliente
    public Cliente save(Cliente entity) {
        return clienteRepository.save(entity);
    }

    //Borra un recurso cliente
    public boolean delete(Cliente entity) {
        try {
            clienteRepository.delete(entity);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }

    public void agregar(Cliente p) {
        p.setId(null);
//        this.repositorioProducto.insert(p);
        this.clienteRepository.insert(p);
    }

    public void guardarPolizaPorId(String idCliente, Cliente cliente) {
        cliente.setId(idCliente);
        this.clienteRepository.save(cliente);
    }

    public void borrarPolizaPorId(String idCliente, String idPoliza, Cliente cliente){
        cliente.setId(idCliente);

        List<Poliza> listpoliza = cliente.getNumero_poliza();
        Poliza poliza = new Poliza();
        List<Poliza> nuevaLista =  new ArrayList<>();

        for(int i=0; i < listpoliza.size(); i++){
            System.out.println("Get poliza: " + listpoliza.get(i).getNumero_poliza());
//            if(listpoliza.get(i).getNumero_poliza() == numero_poliza){
            if(!listpoliza.get(i).getNumero_poliza().equals(idPoliza)){
                System.out.println("Print dentro del if "+poliza.getNumero_poliza());
//                poliza = listpoliza.get(i);
                nuevaLista.add(listpoliza.get(i));
            }
        }

        System.out.println("lista nueva:");
        for(int i = 0; i < nuevaLista.size(); i++){
            System.out.println(nuevaLista.get(i).getNumero_poliza());
        }

//        if(!poliza.getNumero_poliza().equals(idPoliza)){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
//        }

        cliente.setListpoliza(nuevaLista);
        this.clienteRepository.save(cliente);
//
//        System.arraycopy(listpoliza,);
    }

//    //Busca un cliente en especifico
//    //TODO Buscar el cliente dentro de la lista del ejecutivo
//    public Optional<Poliza> findByIdPoliza(String  idCliente, String idPoliza) {
//        return clienteRepository.
//    }


}
