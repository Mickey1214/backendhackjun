package techu.hack.poliza.modelo;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
    @Id String id;
    String nombre;
    String telefono;
    String direccion;
    List<Poliza> listpoliza = new ArrayList<>();

    public Cliente() {
    }

    public Cliente(String id, String nombre, String telefono, String direccion, List<Poliza> listpoliza) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.listpoliza = listpoliza;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Poliza> getNumero_poliza() {
        return listpoliza;
    }

    public void setNumero_poliza(Poliza numero_poliza) {
//        this.numero_poliza = numero_poliza;
        this.listpoliza.add(numero_poliza);
    }

    public void setListpoliza(List<Poliza> listpoliza){
        this.listpoliza = listpoliza;
    }

//    public Poliza getPolizaID(String index, Cliente cliente){
//        Poliza poliza;
//
//
//        return poliza;
//    }
}
