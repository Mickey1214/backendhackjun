package techu.hack.poliza.modelo;

import java.time.LocalDate;
import java.util.Random;

public class Poliza {
    String numero_poliza;
    String fec_inivig;
    String fec_finvig;

    //Constructor
    public Poliza() {
        //Generar la poliza
        Random random = new Random();
        StringBuffer r = new StringBuffer();

        for(int i = 0; i < 10; i++){
            //Numero random del 0 al 10
            int nextRandomChar = 0
                    + (int)(random.nextFloat()
                    * (10));

            r.append(String.valueOf(nextRandomChar));
        }
//        System.out.println("Poliza:" + r.toString());
//        ===================================================

        LocalDate fechInic = LocalDate.now();
        LocalDate fechFin = fechInic.plusYears(1);

//        System.out.println("Fecha inicio: " + fechInic);
//        System.out.println("Fecha fin: " + fechFin);

        this.numero_poliza = r.toString();
        this.fec_inivig = fechInic.toString();
        this.fec_finvig = fechFin.toString();
    }

    public Poliza generarPoliza(){
        //Generar la poliza
        Random random = new Random();
        StringBuffer r = new StringBuffer();

        for(int i = 0; i < 10; i++){
            //Numero random del 0 al 10
            int nextRandomChar = 0
                    + (int)(random.nextFloat()
                    * (10));

            r.append(String.valueOf(nextRandomChar));
        }
//        System.out.println("Poliza:" + r.toString());
//        ===================================================

        LocalDate fechInic = LocalDate.now();
        LocalDate fechFin = fechInic.plusYears(1);

//        System.out.println("Fecha inicio: " + fechInic);
//        System.out.println("Fecha fin: " + fechFin);

//        this.numero_poliza = r.toString();
//        this.fec_inivig = fechInic.toString();
//        this.fec_finvig = fechFin.toString();

        Poliza poliza = new Poliza();
        poliza.setNumero_poliza(r.toString());
        poliza.setFec_inivig(fechInic.toString());
        poliza.setFec_finvig(fechFin.toString());

        return poliza;
    }

    public Poliza(String numero_poliza, String fec_inivig, String fec_finvig) {
        this.numero_poliza = numero_poliza;
        this.fec_inivig = fec_inivig;
        this.fec_finvig = fec_finvig;
    }

    public String getNumero_poliza() {
        return numero_poliza;
    }

    public void setNumero_poliza(String numero_poliza) {
        this.numero_poliza = numero_poliza;
    }

    public String getFec_inivig() {
        return fec_inivig;
    }

    public void setFec_inivig(String fec_inivig) {
        this.fec_inivig = fec_inivig;
    }

    public String getFec_finvig() {
        return fec_finvig;
    }

    public void setFec_finvig(String fec_finvig) {
        this.fec_finvig = fec_finvig;
    }
}
