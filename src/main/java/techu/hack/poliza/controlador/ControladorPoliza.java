package techu.hack.poliza.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.hack.poliza.modelo.Cliente;
import techu.hack.poliza.modelo.Poliza;
import techu.hack.poliza.servicio.ServicioCliente;

import java.util.List;

@RestController
@RequestMapping("/apihack/v2/clientes/{id}/polizas")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class ControladorPoliza {
    @Autowired
    ServicioCliente servicioCliente;

    @GetMapping
    public List<Poliza>getPolizas(@PathVariable String id){
        Cliente cliente = this.servicioCliente.findById(id);
        if(cliente == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return servicioCliente.findAllPolizas(id);
    }

    @GetMapping("/{numero_poliza}")
    public Poliza getPolizaId(@PathVariable String id, @PathVariable String numero_poliza){
        //TODO tirar exception cuando no se encuentra el cliente o la poliza
        Cliente cliente = this.servicioCliente.findById(id);
        if(cliente == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }


        List<Poliza> listpoliza = cliente.getNumero_poliza();
        Poliza poliza = new Poliza();

        for(int i=0; i < listpoliza.size(); i++){
            System.out.println("Get poliza: " + listpoliza.get(i).getNumero_poliza());
//            if(listpoliza.get(i).getNumero_poliza() == numero_poliza){
            if(listpoliza.get(i).getNumero_poliza().equals(numero_poliza)){
                System.out.println("Print dentro del if "+poliza.getNumero_poliza());
                poliza = listpoliza.get(i);
            }
        }

        if(!poliza.getNumero_poliza().equals(numero_poliza)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }else {
            return poliza;
        }
    }

    @PostMapping
    public Poliza postAgregarPolizas(@PathVariable String id){
        //TODO sacar el cliente con el id
        Cliente cliente = servicioCliente.findById(id);

        if(cliente == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        Poliza pol = new Poliza();
        cliente.setNumero_poliza(pol);

        //Insertarlo en la bd
        servicioCliente.guardarPolizaPorId(cliente.getId(), cliente);
        return pol;
    }

//    @PutMapping("/{numero_poliza}")
//    public void putPolizas(@PathVariable String id,
//                           @PathVariable String numero_poliza,
//                           @RequestBody Poliza polizaToUpdate){
////        servicioPoliza.save(polizaToUpdate);
//
//    }

    @DeleteMapping("/{numero_poliza}")
    public boolean deletePolizas(@PathVariable String id, @PathVariable String numero_poliza){
//        return servicioPoliza.delete(polizaToDelete);
        Cliente cliente = servicioCliente.findById(id);
//        if(cliente == null){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
//        }

        servicioCliente.borrarPolizaPorId(id,numero_poliza,cliente);
        return true;
    }
}
