package techu.hack.poliza.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.hack.poliza.modelo.Cliente;
import techu.hack.poliza.modelo.Poliza;
import techu.hack.poliza.servicio.ServicioCliente;

import java.util.List;

@RestController
@RequestMapping("/apihack/v2")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class ControladorCliente {
    @Autowired
    ServicioCliente servicioCliente;

    @GetMapping("/clientes")
    public List<Cliente>getClientes(){
        return servicioCliente.findAll();
    }

    @GetMapping("/clientes/{id}")
    public Cliente getClienteId(@PathVariable String id){
        Cliente cliente = servicioCliente.findById(id);
        if(cliente != null){
            return servicioCliente.findById(id);
        }
        else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/clientes")
    public Cliente postClientes(@RequestBody Cliente newCliente){
        //Agregar un id incremental
        int cantidad = servicioCliente.findAll().size() + 1;
        newCliente.setId(String.valueOf(cantidad));
        newCliente.setNumero_poliza(new Poliza());
        servicioCliente.save(newCliente);
        return newCliente;
    }

    @PutMapping("/clientes/{id}")
    public void putClientes(@PathVariable String id, @RequestBody Cliente cliente){
        //        newCliente.setNumero_poliza(pol.getNumero_poliza());
        Cliente clienteToUpdate = servicioCliente.findById(id);
//        System.out.println("Cliente id: " + clienteToUpdate.getId());
//        System.out.println("Numero de poliza" + cliente.getNumero_poliza());
        if(clienteToUpdate == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        else{
            clienteToUpdate.setNombre(cliente.getNombre());
            clienteToUpdate.setDireccion(cliente.getDireccion());
            clienteToUpdate.setTelefono(cliente.getTelefono());
            servicioCliente.save(clienteToUpdate);
        }
    }

    @DeleteMapping("/clientes/{id}")
    public boolean deleteClientes(@PathVariable String id){
        Cliente cliente = servicioCliente.findById(id);
        if(cliente != null){
            return servicioCliente.delete(cliente);
        }
        else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }
}
